-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Mar 29, 2019 at 10:56 AM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 7.0.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tmc_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `std_users`
--

CREATE TABLE `std_users` (
  `id` int(11) NOT NULL,
  `student_id` varchar(25) NOT NULL,
  `password` varchar(25) NOT NULL,
  `created_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `std_users`
--

INSERT INTO `std_users` (`id`, `student_id`, `password`, `created_at`) VALUES
(1, '11-000039', 'pass', '2019-03-07'),
(2, '11-000040', 'password', '2019-03-08'),
(3, '15-0003875', 'pass', '2019-03-01'),
(4, '15-004145', 'pass', '2019-03-08'),
(5, '14-001555', 'p', '2019-03-07'),
(6, '15-003689', 'pass', '2019-03-06');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `std_users`
--
ALTER TABLE `std_users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `student_id` (`student_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `std_users`
--
ALTER TABLE `std_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
