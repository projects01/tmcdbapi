<?php

namespace Controller;

use Model\model;
use View\view;
use Session\session;
use Authentication\authentication;
use Interfaces\Controller\Lib\Controller_Interface;

class Controller implements Controller_Interface {
       
    protected $execute;
    protected $authentication;

    public function __Construct(){
   
   }

   public function Model($modelPath){

        $model = new Model($modelPath);
        $this->execute = $model->getModelInstance();
        return $this->execute;
   }       

}
?>