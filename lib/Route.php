<?php

namespace Route;
use Interfaces\Route\Lib\Route_Interface;
use File\Check;

class Route implements Route_Interface {
	/**
		route default constructor having url parameter
	**/
	private $backslash = '';
	private $slash;
	private $filename = NULL;

	public function __Construct($url){
		/**
			route default method
		**/
	   $this->trimUrl($url);
	}

	/**
		method trim url and explode values.
	**/
	private function trimUrl($url){
		$this->slash = substr_count($url, '/');
		$url = rtrim($url,'/');

		//this method put values in an array.
	    $url = explode('/', $url);

	    //this method create controller.
		$this->ControllerMethod($url); 
	}

	private function count_forwardSlash(){

		for($i = 0; $i < $this->slash; $i++){
			$this->backslash = $this->backslash . '../';
		}
			
	}

	private function ControllerMethod($url){	
		/**
			count the lengnt of an array,
			array(0) = controller class
			array(1) = controller method;
			array(2) = data
		**/

		$this->count_forwardSlash();

		//check if the url is null
		if($url[0] == null){

			// if true $arraylent set to 0
			$Arraylenght = 0;
		}else{

			// if false $arraylenght set to lenght of an $url
			$Arraylenght = count($url);
		}
		
		//check url lenght to determine what class and method to execute
		$this->filename = 'controller/'.$url[0].'Controller.php';

		if($Arraylenght != 3){

			if($Arraylenght == 1){

				//check if the controller file exist.
				if(Check::if_File_exist($this->filename) === true){

					//require the controller given in the first array.
					require_once'controller/'.$url[0].'Controller.php';
					
					//instantiate the controller class, class is must the same name to its filename.
					$instance = new $url[0]($url[0], $this->backslash);
					$defaultMethod = $url[0];
					$instance->$defaultMethod();

				}else{
					//echo "controller did not exist";
					$this->error_Controller_File();
				}
				
				
			}else if($Arraylenght == 0){

					$this->backslash ='';
					//require the controller given in the first array.
					require_once'controller/loginController.php';
					
					//nstantiate the controller class, class must the same name to its filename.
					$instance = new $url[0]($url[0], $this->backslash);
					$defaultMethod = $url[0];
					$instance->$defaultMethod();

			}else if($Arraylenght == 2){
				
				//convert the array value to string.
				$method = json_encode($url[1]);

				//emove whitespace and other predefined characters from both sides of a string.
				$method = trim($method,'"');

				if(Check::if_File_exist($this->filename) === true){

					//require the controller given in the first array
					require_once'controller/'.$url[0].'Controller.php';
						
						//instantiate the controller class, class must the same name to its filename
						$instance = new $url[0]($url[0], $this->backslash);

						//access the method provided in the second array
						if(Check::if_method_exist($url[0], $method) === true){
						
							$instance->$method();
						}else{

							//Incorrect URL
							$this->error_Controller_File();
						}

				}else{

					$this->error_Controller_File();
				}	

			}else{
				$this->error_Controller_File();	
			}
		
		}else{

			//convert the array value to string.
			$method = json_encode($url[1]);

			//remove whitespace and other predefined characters from both sides of a string
			$method = trim($method,'"');
			$method = $method.'_withParam';
			
			//check if the controller file exist
			if(Check::if_File_exist($this->filename) === true){

				//require the controller given in the first array.
				require_once'controller/'.$url[0].'Controller.php';

					//nstantiate the controller class, class must the same name to its filename
					$instance = new $url[0]($url[0], $this->backslash);/* pass the lenght of the array in constructor*/
					
					//access the method provided in the second array and pass value on it.
					if(Check::if_method_exist($url[0], $method) === true){
						
						$instance->$method($url[2]);

					}else{

						//Incorrect URL
						$this->error_Controller_File();
					}
						
			}else{
					//controller did not exist
					$this->error_Controller_File();
				}	
		}
			      			
	}

	private function error_Controller_File(){
		/**
			url error catcher
		**/
		echo '<div>'; 
		echo '<center><h1>Error 405</h1></center><br/><br/>';
		echo '<br/>';
		echo ' <center><h2>You entered incorect url! please enter correct url address.</h2></center>';
		echo '</div>';
	}

}

?>
