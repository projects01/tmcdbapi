<?php
use Interfaces\Db\Lib\Db_Interface;

class Db implements Db_Interface {
	private static $connection = NULL;

	public function __Construct(){
	}

	public static function getInstance() {

	$database = new database_configuration();
	$myHost =  $database->getDbConfiguration()['host'];
	$myUserName =  $database->getDbConfiguration()['username'];
	$myPassword =  $database->getDbConfiguration()['password'];
	$myDataBaseName =  $database->getDbConfiguration()['database'];

      if (!isset(self::$connection)) { 
      		$con = mysqli_connect( "$myHost", "$myUserName", "$myPassword", "$myDataBaseName" );
				if( !$con )
					{ 
		   			 	die("connection object not created: ".mysqli_error($con));
					}	
				if( mysqli_connect_errno() )
					{ 		   			
		    			die("Connect failed: ".mysqli_connect_errno()." : ". mysqli_connect_error());
					}
        self::$connection = $con;
      }    
      return self::$connection;
    }
}
?>
