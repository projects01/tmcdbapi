<?php

class required{

	public function __Construct(){
		
		$directories = array('interfaces/libraries', 
							 'lib'
							);
		$handler = '';
		$file = '';

		foreach($directories as $dir){
			if ($handler = opendir($dir)) {
			    while (false !== ($file = readdir($handler))) { 
			        if (is_file($dir .'/'. $file)) {
			            require_once $dir.'/'.$file;
			        }
			    }
			    closedir($handler);
			}
		}	
		require_once'config/database.php';
	}
}

$require = new required();

?>