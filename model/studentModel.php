<?php

class studentModel {

	public function __Construct(){

	}

	public function query(){
		$sql = "SELECT * FROM sys_users";
		$result = mysqli_query(Db::getInstance(), $sql);
			if($result->num_rows){
				while($row = $result->fetch_object()){
					$Merchandise[] = $row;
				}
			}else{
				$Merchandise = array();
			}
			
		print json_encode($Merchandise);
	}

	public function login(){
			$username = $_POST["username"];
			$userpass = $_POST["password"];
			 // $username = "11-000039";
			 // $userpass = "pass";

			$sql = "SELECT stu.id, 
						   stu.student_id, 
						   stu.password, 
						   stu.created_at,
						   student.sysid, 
						   student.firstname, 
						   student.lastname, 
						   student.middlename, 
						   student.suffix, 
						   student.addrstreet AS street, 
						   student.addrbrgy AS barrangay, 
						   student.addrmonic AS municipality, 
						   student.addrcity AS city, 
						   student.birth, 
						   student.age,
						   student.sex, 
						   student.citizenship, 
						   student.yearlevel, 
						   student.status,
						   student.cur AS curval,
						   tsc.courseid As courseid
					FROM std_users AS stu 
					LEFT JOIN tbl_students as student ON student.studid = stu.student_id 
					LEFT JOIN tbl_students_course AS tsc ON tsc.studid = student.sysid
					WHERE student_id = '$username' AND password = '$userpass' GROUP BY student.sysid";

			$result = mysqli_query(Db::getInstance(), $sql);
			if($result->num_rows){
				while($row = $result->fetch_object()){
					$credentials[] = $row;
				}
				$status = "success";
			}else{
				$credentials = array();
				$status = "unsuccess";
			}
			
			$data = array("credentials" => $credentials,
						  "status" => $status
						 );
			
			print json_encode($data);
	}	

	public function grade(){
		 
		    $sysid = $_POST['Sysid'];
		    $semester = $_POST['semester'];
		    if($semester == "summer"){ $semester = 3;}
		    $start = $_POST['starts'];
		    $end = $_POST['ends'];
		    $status = $_POST['status'];

		    if($semester == "wholeyear"){
		    	$sql = "SELECT 	tsub.code as subject,
							ttss.gradeprelim AS prelim, ttss.grademidterm AS midterm, ttss.gradeprefinals AS semiFinal, ttss.gradefinals AS final, ttss.grade as grade, ttss.reex as reex
							FROM tbl_trn_stud_schedule as ttss
							INNER JOIN tbl_stud_schedule_main as tssm ON tssm.sysid = ttss.schedid
							INNER JOIN tbl_subjects as tsub ON tsub.sysid = tssm.subjid
							INNER JOIN tbl_students as ts ON ts.sysid = ttss.studid
							INNER JOIN tbl_students_course as tsc ON tsc.studid = ttss.studid
							INNER JOIN tbl_course as tc ON tc.sysid = tsc.courseid
							WHERE tssm.yearstart = $start AND tssm.yearend = $end
							AND tsc.status = $status AND (ttss.status = $status OR ttss.status = 6)
							AND ts.sysid = $sysid";
		    }else if($semester == "all"){
		    	$sql = "SELECT 	tsub.code as subject,
							ttss.gradeprelim AS prelim, ttss.grademidterm AS midterm, ttss.gradeprefinals AS semiFinal, ttss.gradefinals AS final, ttss.grade as grade, ttss.reex as reex
							FROM tbl_trn_stud_schedule as ttss
							INNER JOIN tbl_stud_schedule_main as tssm ON tssm.sysid = ttss.schedid
							INNER JOIN tbl_subjects as tsub ON tsub.sysid = tssm.subjid
							INNER JOIN tbl_students as ts ON ts.sysid = ttss.studid
							INNER JOIN tbl_students_course as tsc ON tsc.studid = ttss.studid
							INNER JOIN tbl_course as tc ON tc.sysid = tsc.courseid
							WHERE tsc.status = $status AND (ttss.status = $status OR ttss.status = 6)
							AND ts.sysid = $sysid";
		    }else{
		    	$sql = "SELECT 	tsub.code as subject,
							ttss.gradeprelim AS prelim, ttss.grademidterm AS midterm, ttss.gradeprefinals AS semiFinal, ttss.gradefinals AS final, ttss.grade as grade, ttss.reex as reex
							FROM tbl_trn_stud_schedule as ttss
							INNER JOIN tbl_stud_schedule_main as tssm ON tssm.sysid = ttss.schedid
							INNER JOIN tbl_subjects as tsub ON tsub.sysid = tssm.subjid
							INNER JOIN tbl_students as ts ON ts.sysid = ttss.studid
							INNER JOIN tbl_students_course as tsc ON tsc.studid = ttss.studid
							INNER JOIN tbl_course as tc ON tc.sysid = tsc.courseid
							WHERE tssm.yearstart = $start AND tssm.yearend = $end AND tssm.semester = $semester
							AND tsc.status = $status AND (ttss.status = $status OR ttss.status = 6)
							AND ts.sysid = $sysid";
		    }
		    
			$result = mysqli_query(Db::getInstance(), $sql);
			if($result->num_rows){
				while($row = $result->fetch_object()){
					$grade[] = $row;
				}
			}else{
				$grade = array();
			}
			
			return json_encode($grade);
	}


	public function prospectus(){

		$year = $_POST["year"];
		$sem = $_POST["sem"];
		$sid = $_POST["Sysid"];
		$curval = $_POST["curval"];
		$courseid = $_POST["courseid"];

		if($year != 0){
			if($sem != 0){
				$sql = "SELECT ts.code AS subject, ts.descriptions, tsp.courselevel AS level, tsp.semester
					FROM tbl_subjects_prospectus AS tsp
					INNER JOIN tbl_subjects AS ts ON ts.sysid = tsp.subjid
					WHERE tsp.courseid = $courseid AND tsp.curval = $curval 
					AND tsp.courselevel = $year AND tsp.semester = $sem
					GROUP BY ts.code
					ORDER BY tsp.courselevel, tsp.semester, ts.code ASC";
			}else{
				$sql = "SELECT ts.code AS subject, ts.descriptions, tsp.courselevel AS level, tsp.semester
					FROM tbl_subjects_prospectus AS tsp
					INNER JOIN tbl_subjects AS ts ON ts.sysid = tsp.subjid
					WHERE tsp.courseid = $courseid AND tsp.curval = $curval 
					AND tsp.courselevel = $year
					GROUP BY ts.code
					ORDER BY tsp.courselevel, tsp.semester, ts.code ASC";
			}

		}else{
			$sql = "SELECT ts.code AS subject, ts.descriptions, tsp.courselevel AS level, tsp.semester
					FROM tbl_subjects_prospectus AS tsp
					INNER JOIN tbl_subjects AS ts ON ts.sysid = tsp.subjid
					WHERE tsp.courseid = $courseid AND tsp.curval = $curval 
					AND (tsp.semester = 1 OR tsp.semester = 2) 
					GROUP BY ts.code
					ORDER BY tsp.courselevel, tsp.semester, ts.code ASC";
		}

			$result = mysqli_query(Db::getInstance(), $sql);
			if($result->num_rows){
				while($row = $result->fetch_object()){
					$prospectus[] = $row;
				}
			}else{
				$prospectus = array();
			}
			
			return json_encode($prospectus);
	}


	public function taken(){
		$courseid = $_POST["courseid"];
		$curval = $_POST["curval"];
		$id = $_POST['Sysid'];
		$status = $_POST['status'];
		$sql = "SELECT 	tsub.code as subject, tsub.descriptions AS description, ttss.grade as grade, ttss.reex as reex
						FROM tbl_trn_stud_schedule as ttss
						INNER JOIN tbl_stud_schedule_main as tssm ON tssm.sysid = ttss.schedid
						INNER JOIN tbl_subjects as tsub ON tsub.sysid = tssm.subjid
						INNER JOIN tbl_students as ts ON ts.sysid = ttss.studid
						INNER JOIN tbl_students_course as tsc ON tsc.studid = ttss.studid
						INNER JOIN tbl_course as tc ON tc.sysid = tsc.courseid
						WHERE tsc.status = 1 AND (ttss.status = 1 OR ttss.status = 6)
						AND ts.sysid = $id 
						GROUP BY tsub.code
						ORDER BY tsub.code ASC";
			$result = mysqli_query(Db::getInstance(), $sql);
			if($result->num_rows){
				while($row = $result->fetch_object()){
					$taken[] = $row;
				}
			}else{
				$taken = array();
			}
			
			return json_encode($taken);
	}

	public function unenrolled(){
		//  $id = 6299;
		// $cid = 1;
		// $curval = 1;
		// $stats = 1;
		$id = $_POST['Sysid'];
		$cid = $_POST['courseid'];
		$curval = $_POST['curval'];
		$stats = $_POST['status'];

		$sql = "SELECT tss.code AS subject, tss.descriptions AS description, tspp.courselevel AS level, tspp.semester
					FROM tbl_subjects_prospectus AS tspp
					INNER JOIN tbl_subjects AS tss ON tss.sysid = tspp.subjid
					WHERE NOT EXISTS(SELECT * FROM tbl_trn_stud_schedule as ttss
                                        INNER JOIN tbl_stud_schedule_main AS tssm ON tssm.sysid = ttss.schedid
                                        INNER JOIN tbl_subjects as tsub ON tsub.sysid = tssm.subjid
                                        INNER JOIN tbl_students as ts ON ts.sysid = ttss.studid
                                        INNER JOIN tbl_students_course as tsc ON tsc.studid = ttss.studid
                                        INNER JOIN tbl_course as tc ON tc.sysid = tsc.courseid
                                        WHERE tsc.status = $stats
                                        AND ts.sysid = $id AND tsub.sysid = tss.sysid AND tssm.subjid = tss.sysid)
					AND tspp.courseid = $cid AND tspp.curval = $curval
					GROUP BY tss.code
					ORDER BY tspp.courselevel, tspp.semester, tss.code ASC";
    
			$result = mysqli_query(Db::getInstance(), $sql);
			if($result->num_rows){
				while($row = $result->fetch_object()){
					$unenrolled[] = $row;
				}
			}else{

				$unenrolled = array();
			}
			return json_encode($unenrolled);
	}

}
?>
