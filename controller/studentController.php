<?php
	use controller\Controller;

	class student extends Controller {
		private $controller;
		private $url;
		private $data;

		public function __Construct($url, $backslash){
			$this->controller = new controller();
			$this->backslash = $backslash;
			$this->url = $url;
		}

		public function student(){}
		public function save(){}
		public function query(){
			$data = $this->controller->Model($this->url)->query();
		}

		public function login(){
			$data = $this->controller->Model($this->url)->login();
		}

		public function grade(){
			$data = $this->controller->Model($this->url)->grade();
			print $data;
		}

		

		public function unenrolled(){
			$data = $this->controller->Model($this->url)->unenrolled();
			print $data;
		}

		public function prospectus(){
			$data = $this->controller->Model($this->url)->prospectus();
			print $data;
		}

		public function taken(){
			$data = $this->controller->Model($this->url)->taken();
			print $data;
		}

		public function client(){
			
		}

	}


?>